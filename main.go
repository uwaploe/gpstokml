// Gpstokml generates a KML file from one or more SBF PVT files from a DOT buoy
package main

import (
	"bufio"
	"encoding/binary"
	"flag"
	"fmt"
	"image/color"
	"io"
	"log"
	"math"
	"os"
	"runtime"
	"strconv"
	"time"

	sbf "bitbucket.org/uwaploe/go-sbf"
	kml "github.com/twpayne/go-kml"
	"github.com/twpayne/go-kml/icon"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: gpstokml [options] file [file...]

Generate a KML file from one or more SBF PVT files from a DOT buoy. Any input
filenames starting with "@" are assumed to contain a list of SBF files, one
name per line.
`

type Color struct {
	color.RGBA
}

func (c *Color) String() string {
	if c != nil {
		return fmt.Sprintf("#%02x%02x%02x%02x",
			c.A, c.R, c.G, c.B)
	}
	return ""
}

func (c *Color) Set(text string) error {
	var err error
	if string(text[0]) != "#" {
		return fmt.Errorf("Invalid color specification: %q", text)
	}
	x, err := strconv.ParseUint(string(text[1:]), 16, 32)
	if err != nil {
		return err
	}
	c.A = uint8(x >> 24)
	c.R = uint8((x >> 16) & 0xff)
	c.G = uint8((x >> 8) & 0xff)
	c.B = uint8(x & 0xff)
	return nil
}

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	outFile  = flag.String("output", "", "Output file name")
	dotId    string
	kmlTitle string
	trkColor Color
	noTrack  bool
)

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

type position struct {
	time.Time
	kml.Coordinate
}

func getPositions(filename string) ([]position, error) {
	fin, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("open %q: %w", filename, err)
	}
	defer fin.Close()

	var rec sbf.PvtGeodetic

	positions := make([]position, 0)
	scanner := sbf.NewScanner(fin)
	for scanner.Scan() {
		if b := scanner.Block(); b.BlockNum() == 4007 {
			err = binary.Read(b.Reader(), sbf.ByteOrder, &rec)
			if err != nil {
				return positions, err
			}
			if rec.Error != 0 {
				continue
			}

			p := position{}

			p.Time = rec.T.Time()
			p.Coordinate = kml.Coordinate{
				Lon: rec.Lon * 180. / math.Pi,
				Lat: rec.Lat * 180. / math.Pi,
				Alt: rec.Height - float64(rec.Undulation),
			}
			positions = append(positions, p)
		}
	}

	return positions, scanner.Err()
}

func readFiles(listfile string) ([]position, error) {
	fin, err := os.Open(listfile)
	if err != nil {
		return nil, fmt.Errorf("open %q: %w", listfile, err)
	}
	defer fin.Close()

	positions := make([]position, 0)
	scanner := bufio.NewScanner(fin)
	for scanner.Scan() {
		pos, err := getPositions(scanner.Text())
		if err != nil {
			return positions, err
		}
		positions = append(positions, pos...)
	}

	return positions, scanner.Err()
}

func addToTrack(pos []position, track *kml.CompoundElement) (time.Time, time.Time) {
	var tStart, tEnd time.Time

	for _, p := range pos {
		tEnd = p.Time
		if tStart.IsZero() {
			tStart = tEnd
		}
		track.Add(kml.When(tEnd), kml.GxCoord(p.Coordinate))
	}

	return tStart, tEnd
}

func makeLineString(pos []position) *kml.CompoundElement {

	coords := make([]kml.Coordinate, 0, len(pos))
	for _, p := range pos {
		coords = append(coords, p.Coordinate)
	}

	return kml.LineString(
		kml.Extrude(false),
		kml.Tessellate(false),
		kml.AltitudeMode("absolute"),
		kml.Coordinates(coords...),
	)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.StringVar(&dotId, "id", lookupEnvOrString("DOT_ID", dotId), "DOT Buoy ID")
	flag.StringVar(&kmlTitle, "title", lookupEnvOrString("KML_TITLE", kmlTitle),
		"KML document name")
	flag.BoolVar(&noTrack, "no-track", noTrack,
		"display locations as a placemark rather than a track")
	flag.Var(&trkColor, "color", "Track color")
	pc := &trkColor
	pc.Set(lookupEnvOrString("KML_TRACK_COLOR", "#ff00ff00"))
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		fout io.WriteCloser
		err  error
	)

	if *outFile != "" {
		fout, err = os.OpenFile(*outFile, os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatalf("Cannot create output file: %v", err)
		}
	} else {
		fout = os.Stdout
	}

	track := kml.GxTrack(
		kml.AltitudeMode("absolute"),
	)

	doc := kml.Document(
		kml.Name(kmlTitle),
		kml.SharedStyle(
			"buoyStyle",
			kml.IconStyle(
				kml.Icon(kml.Href(icon.TrackHref(0))),
				kml.Color(trkColor.RGBA),
			),
			kml.LineStyle(
				kml.Width(1.0),
				kml.Color(trkColor.RGBA),
			),
		),
	)

	var (
		tStart, tEnd, t0 time.Time
	)

	positions := make([]position, 0)
	for _, arg := range args {
		if arg[0:1] == "@" {
			pos, err := readFiles(arg[1:])
			if err != nil {
				log.Fatal(err)
			}
			t0, tEnd = addToTrack(pos, track)
			positions = append(positions, pos...)
		} else {
			pos, err := getPositions(arg)
			if err != nil {
				log.Fatal(err)
			}
			t0, tEnd = addToTrack(pos, track)
			positions = append(positions, pos...)
		}

		if tStart.IsZero() {
			tStart = t0
		}
	}

	if noTrack {
		doc.Add(
			kml.Placemark(
				kml.Name(dotId+" Positions"),
				kml.Description(
					fmt.Sprintf("%s positions from %s until %s",
						dotId, tStart, tEnd)),
				kml.StyleURL("#buoyStyle"),
				makeLineString(positions),
			),
		)
	} else {
		doc.Add(
			kml.Placemark(
				kml.Name(dotId+" Track"),
				kml.Description(
					fmt.Sprintf("%s track from %s until %s",
						dotId, tStart, tEnd)),
				kml.StyleURL("#buoyStyle"),
				track,
			),
		)
	}

	k := kml.GxKML(
		doc,
	)

	if err := k.WriteIndent(fout, "", "  "); err != nil {
		log.Fatal(err)
	}
}
