module bitbucket.org/uwaploe/gpstokml

go 1.18

require (
	bitbucket.org/uwaploe/go-sbf v0.5.0
	github.com/pkg/errors v0.8.0
	github.com/twpayne/go-kml v1.2.0
)

require github.com/twpayne/go-polyline v1.0.0 // indirect
